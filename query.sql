use AkifcanKara

-- GO
-- CREATE PROCEDURE GET_STUDENTS
-- AS
-- SELECT * FROM Ogrenci

-- EXEC GET_STUDENTS

-- GO
-- ALTER PROCEDURE GET_STUDENTS_BY_CITY @City nvarchar(30)
-- AS
-- BEGIN
-- SELECT * FROM Ogrenci where Sehir = @City
-- END

-- DECLARE @TestCity NVARCHAR(50);
-- SET @TestCity = 'seul';

-- EXEC GET_STUDENTS_BY_CITY @City = @TestCity


/*GO
ALTER TRIGGER trigger_logger
ON Ogrenci
AFTER INSERT
AS
BEGIN
insert into Logs (Process, addedStudentId) VALUES('NEW DATA ADDED',  IDENT_CURRENT('ogrenci'))
END*/

/* SELECT  
    name,
    is_instead_of_trigger
FROM
    sys.triggers  
WHERE
    type = 'TR'; */

/*GO  
DISABLE TRIGGER trigger_logger ON Ogrenci;  
GO*/  

/*GO
ENABLE TRIGGER trigger_logger ON Ogrenci;  
GO*/

--insert into Bolum (Ad) VALUES('DB')
-- insert into Ogrenci (Ad, Soyad, BolumNo, Sehir, NameLength) VALUES ('insrted', 'VERNE', 1, 'FRANCE', LEN('JULES'))
-- SELECT * FROM Ogrenci
--INNER JOIN Bolum ON Bolum.BolumNo=Ogrenci.BolumNo;*/

-- Select * FROM Ogrenci
--Select * from Bolum
--Select Ad as Name, Soyad as Lastname, Sehir as City from Ogrenci WHERE Sehir IN ('seul', 'bornova')

--Select * FROM Ogrenci WHERE Score BETWEEN 50 AND 100

-- SELECT COUNT(Score) FROM Ogrenci

-- SELECT DISTINCT Ad, Soyad as Lastname FROM Ogrenci;

/*SELECT COUNT(Sehir) as Member, Sehir
FROM Ogrenci
GROUP BY Sehir
HAVING COUNT(Sehir) >= 2
ORDER BY COUNT(Sehir) DESC;
*/

-- SELECT DAY('2017/08/25') AS DayOfMonth;

-- ALTER TABLE Logs
-- ADD created_at datetime DEFAULT CURRENT_TIMESTAMP;

-- SELECT USER_NAME();

/* GO
CREATE VIEW [BORNOVA STUDENTS] AS
SELECT *
FROM Ogrenci
WHERE Sehir = 'bornova';
*/

/* GO
CREATE VIEW [CITIES GROUP] AS
SELECT COUNT(Sehir) as Member, Sehir
FROM Ogrenci
GROUP BY Sehir
HAVING COUNT(Sehir) >= 2
*/

/*select * from ogrenci

SELECT Ad, Soyad, Score, BolumNo
FROM ogrenci
ORDER BY
(CASE
    WHEN Score IS NULL THEN  BolumNo
    ELSE Score
END);
*/
/*SELECT *,
CASE
    WHEN Score IS NOT NULL THEN 'This student has a score'
    WHEN SCORE IS NULL THEN 'This student doesnt have score'
END AS Result
FROM ogrenci;
*/
--SELECT IDENT_CURRENT('ogrenci')